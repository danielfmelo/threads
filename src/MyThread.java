import java.text.DecimalFormat;
import java.util.Random;

public class MyThread implements Runnable {
    private String task;
    private int number;
    private int sleep;
    private int rand;

    public MyThread(String name, int number, int sleep) {

        this.task = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    public void run() {
        System.out.println("\n\nExecuting with these parameters: Name =" + task + " Number = "
                + number + " Sleep = " + sleep + " Rand Num = " + rand + "\n\n");
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                double d = (double) count / rand * 100;
                System.out.println(task + " is " + Math.round(d) + "% completed.");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n\n" + task + " is done.\n\n");
    }
}
