public class TestThread {
    public static void main(String[] args) {
        new Thread(t1).start();
        new Thread(t2).start();
    }
    private static Runnable t1 = () -> {
        for (int i = 0; i < 10; i++){
            System.out.println("Thread 1: " + i);
        }
    };


    private static Runnable t2 = () -> {
        for (int i = 0; i < 10; i++){
            System.out.println("Thread 2: " + i);
        }
    };
}
