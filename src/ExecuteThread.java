import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteThread {
    public static void main(String[] args) throws IOException {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        MyThread ds1 = new MyThread("Bob  ", 25, 1000);
        MyThread ds2 = new MyThread("Sally", 10, 500);
        MyThread ds3 = new MyThread("Billy", 5, 250);
        MyThread ds4 = new MyThread("Anna ", 2, 100);
        MyThread ds5 = new MyThread("Pat  ", 1, 50);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
